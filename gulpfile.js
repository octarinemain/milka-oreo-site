var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var htmlImport = require('gulp-html-import');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var babel = require("gulp-babel");
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin");
var cache = require("gulp-cache");
var pngquant = require('imagemin-pngquant');
var imageminJpegRecompress = require('imagemin-jpeg-recompress');
var cssnano = require('gulp-cssnano');
var spritesmith = require('gulp.spritesmith');
var merge = require('merge-stream');

// SASS/SCSS to CSS
gulp.task('sass', function () {
    gulp.src('src/style/scss/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
		.pipe(cssnano())
        .pipe(gulp.dest('app/css'))
});

//All JavaScript libs to library.min.js
gulp.task('library', function () {
    gulp.src('src/js/library-js/**/*.js')
        .pipe(concat('library.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'));
});

//Browser server
gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'app'
        },
		notify: false,
		ghostMode: false
    });
});

//Main JavaScript babel + uglify
gulp.task('js', function () {
    return gulp.src('src/js/app.js')
        // .pipe(rename({suffix: '.min'}))
        .pipe(babel({presets: ['es2015']}))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'));
});

//Import HTML components
gulp.task('import', function () {
    gulp.src('src/pages/*.html')
        .pipe(htmlImport('src/pages/components/'))
        .pipe(gulp.dest('app'));
});

// Compress IMG
gulp.task('compress', function() {
    return gulp.src('src/img/*')
        .pipe(cache(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imageminJpegRecompress({
                progressive: true,
                min: 70,
                max: 80,
                quality:'veryhigh'
            }),
            imagemin.svgo(),
            imagemin.optipng({optimizationLevel: 7})
        ],{
            verbose: true
        })))
        .pipe(gulp.dest('app/img'));
});

// Cache
gulp.task('clear', function() {
    return cache.clearAll();
});

// Create Sprite
gulp.task('sprite', ['css-libs'], function () {
    var spriteData = gulp.src('src/img/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css',
        imgPath: '../img/sprite/sprite.png'
    }));

    var imgStream = spriteData.img
        .pipe(gulp.dest('app/img/sprite/'));

    var cssStream = spriteData.css
        .pipe(gulp.dest('src/style/library-css/'));

    return merge(imgStream, cssStream);
});

//All CSS libs to library.min.css
gulp.task('css-libs', function() {
    return gulp.src('src/style/library-css/**/*.css')
        .pipe(concat('library.css'))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'));
});

// All Watch
gulp.task('watch', ['browser-sync', 'sass', 'import', 'library', 'js', 'css-libs', 'sprite'], function () {
    gulp.watch('src/style/scss/**/*.scss', ['sass']);
    gulp.watch('src/style/scss/**/*.scss', browserSync.reload);
    gulp.watch('src/**/*.html', ['import']);
    gulp.watch('src/**/*.html', browserSync.reload);
    gulp.watch('src/js/library-js/**/*.js', ['library']);
    gulp.watch('src/js/library-js/**/*.js', browserSync.reload);
    gulp.watch('src/js/app.js', ['js']);
    gulp.watch('src/js/app.js', browserSync.reload);
    gulp.watch('src/style/library-css/**/*.css', ['css-libs']);
    gulp.watch('src/style/library-css/**/*.css', browserSync.reload);
    gulp.watch('src/img/sprite/*.png', ['sprite']);
    gulp.watch('src/img/sprite/*.png', browserSync.reload);
});

// Default Start
gulp.task('default', ['watch']);