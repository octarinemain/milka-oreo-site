$(document).ready(function () {

    // Loader
    $('.loader').addClass('active');
    setTimeout(function () {
        $('.loader').addClass('del');
    }, 1500);

    //PopUp
    function popUp() {
        $('.js-popup-button').on('click', function (e) {
            e.preventDefault();
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });

        // Close PopUp
        $('.js-close-popup').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function (e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }

    popUp();

    function swipVid() {
        const swiperVid = new Swiper('.swiper-video', {
            slidesPerView: 1,
            effect: 'fade',
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
    }

    swipVid();

    function scroll() {
        $(".head__down").on("click", function (e) {
            e.preventDefault();
            var id = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        });
    }

    scroll();

    // Test page
    function test() {
        let sumVal = 0;
        const allLength = $('.testing__card').length;

        $('.testing__answerSelect').on('click', function () {
            const answer = parseInt($(this).attr('data-v'));
            const parent = $(this).closest('.testing__card');

            sumVal += answer;

            parent.removeClass('active');
            parent.index() < (allLength - 1) ? parent.next().addClass('active') : result(sumVal);
        });

        function result(val) {
            $('.testing').css('height', '500px');

            val <= 3 ? location.href = "milka.html" : location.href = "oreo.html";
        }
    }

	test();

	// LazyLoadIfreme
	function LazyLoadIframe() {
		var youtubeWrap = document.getElementById('youtube-video');
		var youtube = youtubeWrap.querySelectorAll('.thumb-wrap');
	
		for (var i = 0; i < youtube.length; i++) {
			var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
			var image = new Image();
				image.src = source;
			image.addEventListener('load', function() {
				youtube[i].appendChild(image);
			}(i));
		
			youtube[i].addEventListener('click', function() {
				var iframe = document.createElement('iframe');
					iframe.setAttribute('frameborder', '0');
					iframe.setAttribute('allow', 'autoplay; encrypted-media');
					iframe.setAttribute('allowfullscreen', '');
					iframe.setAttribute('src', "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&amp;autoplay=1&showinfo=0" );

					this.innerHTML = '';
					this.appendChild(iframe);
			});
		};

		// Autoplay
		function scrollAutoplay() {
			var video = youtube[0];
			var wrapVideo = $('.head');
			var times = 0, playY;
			var scrollPosition = $(window).scrollTop();	
			var positionWrap = 	wrapVideo.offset().top;

			if (scrollPosition >= positionWrap && times == 0) {
				setTimeout(function () {
					playY = video.click();
				}, 3000);
				times = 1;
			}
		}
		scrollAutoplay();
	}
	var hasVideo = $('#youtube-video').length;

	if (hasVideo === 1) {
		LazyLoadIframe();
	}
		

});